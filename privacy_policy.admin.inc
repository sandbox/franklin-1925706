<?php

function privacy_policy_admin_form() {
  $form = array();

  $form['monitoring'] = array(
    '#type' => 'fieldset',
    '#title' => t('Monitoring'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['monitoring']['privacy_policy_notify_actions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Change Response Actions'),
    '#descriptions' => t('Select all the actions the site should take if changes to the Privacy Policy are detected.'),
    '#options' => array(
      'watchdog' => t('Add an entry to watchdog'),
      'dsm_on_login' => t('Notify the user on login.'),
      'email' => t('Send an email to the users listed below.'),
    ),
    '#default_value' => privacy_policy_notify_actions(),
  );

  $form['monitoring']['privacy_policy_notify_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Notify List'),
    '#default_value' => variable_get('privacy_policy_notify_list',''),
    '#description' => t('List the email addresses to notify when a change in the privacy policy is detected, one address per line.'),
  );

  $preamble_text = variable_get('privacy_policy_preamble', array('format' => NULL, 'value' => ''));
  $coda_text = variable_get('privacy_policy_coda', array('format' => NULL, 'value' => ''));

  $form['privacy_policy_preamble'] = array(
    '#title' => t('Preamble'),
    '#type' => 'text_format',
    '#description' => t('A bit of text before the list of policy items.'),
    '#default_value' => $preamble_text['value'],
    '#format' => $preamble_text['format'],
  );

  $form['privacy_policy_coda'] = array(
    '#title' => t('Coda'),
    '#type' => 'text_format',
    '#description' => t('A bit of text after the list of policy items.'),
    '#default_value' => $coda_text['value'],
    '#format' => $coda_text['format'],
  );

  $form['#validate'][] = 'privacy_policy_admin_validate';
  
  return system_settings_form($form);
}

function privacy_policy_admin_validate($form, &$form_state) {
  /* Validate the emails before saving */
  $emails = $form_state['values']['privacy_policy_notify_list'];
  foreach(explode("\n", $emails) as $email_raw) {
    $test_email = trim($email_raw);
    if (!empty($test_email) && !valid_email_address($test_email)) {
      form_set_error('privacy_policy_notify_list', t('Invalid email %email', array('%email' => $test_email)));
    }
  } 
}